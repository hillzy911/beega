package com.beega.inclusivity.beega;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class Info extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

  //      ActionBar actionBar = getSupportActionBar();

//        actionBar.hide();


        setContentView(R.layout.activity_info);
        Button what = (Button)findViewById(R.id.whatIsBeega);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Aclonica.ttf");
        what.setTypeface(font);

        Button create = (Button)findViewById(R.id.createCircle);
        create.setTypeface(font);

        Button join = (Button)findViewById(R.id.joinCircle);
        join.setTypeface(font);



    }

}
