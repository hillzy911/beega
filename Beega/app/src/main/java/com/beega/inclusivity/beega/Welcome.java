package com.beega.inclusivity.beega;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import gr.net.maroulis.library.EasySplashScreen;

public class Welcome extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();

        actionBar.hide();

        EasySplashScreen config = new EasySplashScreen(Welcome.this)
                .withFullScreen()
                .withTargetActivity(Info.class)
                .withSplashTimeOut(4000)
                .withBackgroundResource(R.color.welcome)
                .withHeaderText("")
                .withFooterText("Powered By Inclusivity Solutions")
                .withBeforeLogoText("")
                .withLogo(R.drawable.logo)
                .withAfterLogoText("Beega Circles \n \t Beega Protection \n \t \t Beega Rewards!");


        //set your own animations
        //myCustomTextViewAnimation(config.getFooterTextView());

        //customize all TextViews
        Typeface aclonica = Typeface.createFromAsset(getAssets(), "fonts/Aclonica.ttf");
        config.getAfterLogoTextView().setTypeface(aclonica);

        config.getHeaderTextView().setTextColor(Color.WHITE);
        config.getFooterTextView().setTextColor(Color.WHITE);

        //create the view
        View easySplashScreenView = config.create();

        setContentView(easySplashScreenView);

        //setContentView(R.layout.activity_welcome);
    }

    private void myCustomTextViewAnimation(TextView tv){
        Animation animation=new TranslateAnimation(0,0,480,0);
        animation.setDuration(1200);
        tv.startAnimation(animation);
    }
}
